#!/bin/bash
#
#RUN WITH "./user-creation.sh delete" to delete all accounts.  Otherwise, just run "./user-creation.sh" to create the accounts
#
#Created by Kevin Clark -- COSE IT student worker -- 11/14/18
#Must be run as root due to the account creation
#Outputs a list of temporary passwords and the account name associated with it
#Print this list, give each student a line with one account and password
#Student can then log in, but are forced to change their password immediately

#Create 40 normal accounts and one sudo account
create()
{
	current_date=$(date +"%F-%H:%M:%S")
	for i in {01..41}; do #Generates 40 standard student accounts
		useradd cna397$i #Create student account
		pw=$(head /dev/urandom | md5sum | cut -c4-13) #Generating random passwords
		echo -e "Username: cna397$i   Temporary password: $pw \n" >> /root/accounts${current_date}.txt #Output to file for later printing
		echo $pw | passwd --stdin cna397$i #Set temporary password
		chage -d 0 cna397$i #Expire password immediately
	done

	usermod -aG wheel cna39741 #Add teacher's account to the "wheel" group, giving full sudo access
	echo 'The "cna39741" account is an admin. To switch to the root account, type "sudo -i". Please be careful with root.' >> /root/accounts${current_date}.txt
}

delete()
{
	echo "This might take awhile..."
	for i in {01..41}; do
		userdel -r cna397$i
	done
}

case $1 in
	""|"create")
		create
		;;
	"delete")
		delete
		;;
	*)
		echo "Bad argument"
		;;
esac

